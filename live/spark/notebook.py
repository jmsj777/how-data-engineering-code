# %%
from pyspark.sql.window import Window
from delta.tables import *
from pyspark.sql import functions as F
print(hello world)

# %%

%sql SELECT 1

# %%

%fs ls s3: // belisco-data-lake-raw/dms/app/public/customers

# %%

%md  # Batch

# %%

df = spark.read.format("json").load(
    "s3://belisco-data-lake-raw/mercado-bitcoin/day-summary/")
display(df)
# %%

df.schema

# %%

df = df.withColumn('date', F.to_date('date'))
df.schema

# %%

display(df)

# %%

df.coalesce(1).write.partitionBy(['coin', 'date']).format('parquet').save(
    "s3://belisco-data-lake-processed/mercado-bitcoin_parquet/day-summary/")

# %%

df.coalesce(1).write.partitionBy(['coin', 'date']).format('delta').save(
    "s3://belisco-data-lake-processed/mercado_bitcoin_delta/day-summary/")

# %%

df = spark.format('delta').load(
    "s3://belisco-data-lake-processed/mercado_bitcoin_delta/day-summary/")
df.createOrReplaceTempView('day_summary')

# %%

%sql SELECT * FROM day_summary

# %%

deltaTable = DeltaTable.forPath(
    spark, 's3://belisco-data-lake-processed/mercado_bitcoin_delta/day-summary/')
display(deltaTable.history())  # get full history of the table

# %%

%sql create database mercado_bitcoin
%sql create table mercado_bitcoin.day_summary using delta location 's3://belisco-data-lake-processed/mercado_bitcoin_delta/day-summary/'
%sql SELECT * FROM mercado_bitcoin.day_summary

# %%

%md Streaming

# %%

data_schema = spark.read.format('parquet').load(
    "s3://belisco-data-lake-raw/dms/app/public/customers").schema
data_schema

# %%

display(spark.read.format('parquet').load(
    "s3://belisco-data-lake-raw/dms/app/public/customers"))

# %%

df = spark.read.format('parquet').load(
    "s3://belisco-data-lake-raw/dms/app/public/customers").filter(F.col('Op').isNull())
df = df.withColumn('etl_timestamp', F.current_timestamp())
df = df.withColumn('created_date', F.to_date(F.col('created_at')))
df = df.withColumn('source_filename', F.input_file_name())
df = df.withColumn('row_rank', F.row_number().over(
    Window.partitionBy(F.col('customer_id')).orderBy(F.col('extracted_at').desc())))
df = df.fiter(F.col('row_rank').isin(1))
df = df.drop("row_rank")

df.write.format('delta').mode('overwrite').partitionBy('created_date').save(
    's3://belisco-data-lake-processed/app/customers')

# %%

deltaTable = DeltaTable.forPath(
    spark, 's3://belisco-data-lake-processed/app/customers')


def _upsert(microbatch, batch_id):
    microbatch = microbatch.withColumn('row_rank', F.row_number().over(
        Window.partitionBy(F.col('customer_id')).orderBy(F.col('extracted_at').desc())))
    microbatch = microbatch.fiter(F.col('row_rank').isin(1))
    microbatch = microbatch.drop("row_rank")

    deltaTable.alias('target').merge(microbatch.alias('updated'), 'target.customer_id = updates.customer_id').whenMathedUpdateAll(
        "updates.Op='U'").whenNotMatchedInsertAll("updates.Op='I'").execute()


data_schema = spark.read.format('parquet').load(
    "s3://belisco-data-lake-raw/dms/app/public/customers").schema

df = spark.readStream.format('parquet').schema(data_schema).load(
    's3://belisco-data-lake-raw/dms/public/customers')
df = df.withColumn('etl_timestamp', F.current_timestamp())
df = df.withColumn('created_date', F.to_date(F.col('created_at')))
df = df.withColumn('source_filename', F.input_file_name())
df.writeStream.format('delta').option('checkpointLocation',
                                      "dbfs:/checkpoints/orderv_v2").outputMode('update').foreachBatch(_upsert).start()

# %%

df = spark.read.format('delta').load(
    "s3://belisco-data-lake-processed/app/customers").schema
display(df)

# %%

display(df.filter(F.col('customer_id') == 'some real value from the extract'))

# %%

df.writeStream.format('delta').trigger(once=True).option('checkpointLocation',
                                                         "dbfs:/checkpoints/orderv_v2").outputMode('update').foreachBatch(_upsert).start()
