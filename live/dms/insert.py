from datetime import datetime
import json
import time
from faker import Faker
import psycopg2

faker_instance = Faker()


def get_data():
    lat, lng, region, country, timezone = faker_instance.location_on_land()
    created_datetime= datetime.utcnow()
    return dict(
        created_at=f"{created_datetime}",
        updated_at=f"{created_datetime}",
        customer_id=faker_instance.uuid4(),
        name=faker_instance.name(),
        region=region,
        country=country,
        lat=lat,
        lng=lng,
        email=faker_instance.ascii_free_email(),
        phone=faker_instance.phone_number()
    )

# print(json.dumps(get_data()))

def get_insert_query():
    return """
        insert into customers values (
            '{created_at}',
            '{updated_at}',
            '{customer_id}',
            '{name}',
            '{region}',
            '{country}',
            {lat},
            {lng},
            '{email}',
            '{phone}'
        )
    """.format(**get_data())

# print(get_insert_query())

dsn = (
    "dbname={dbname} "
    "user={user} "
    "password={password} "
    "port={port} "
    "host={host} ".format(
        dbname="",
        user="postgres"
        password="",
        port=5432,
        host="",
    )
)

conn = psycopg2.connect(dsn)
print("connected")
conn.set_session(autocommit=True)
cur = conn.cursor()
cur.execute("""
    create table if not exists customers(
        created_at timestamp,
        updated_at timestamp,
        customer_id uuid PRIMARY KEY,
        name varchar(200),
        region varchar(200),
        country varchar(200),
        lat float,
        lng float,
        email varchar(80),
        phone varchar(30)
    );
""")

while True:
    query = get_insert_query()
    cur.execute(query)
    print(query)
    time.sleep(0.5)
