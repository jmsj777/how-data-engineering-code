terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "ze_aws"
  region  = "us-east-1"
}

resource "aws_s3_bucket" "bucket-production-cleber" {
    bucket = "cleber-s-bucket-production"
    tags = {
      "Area" = "Products"
      "Enviroment" = "Production"
    }
}