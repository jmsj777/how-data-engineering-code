# %%
import pandas as pd
# %%
record = '''
{
    "name":"seilah",
    "after":{
        "a":"432",
        "b":"123123123",
        "c":"23432",
        "d":"3333",
    },
}'''
# %%
df = pd.read_json(record)
frame_after = df.after.to_frame()
print(frame_after)
# %%
parquet = frame_after.to_parquet('after.parquet')
# %%
print(parquet)
go_back = pd.read_parquet('after.parquet')

print(go_back)
