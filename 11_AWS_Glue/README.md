# What is glue: 
- You just can do aws glue jobs for little stuff, for little maintenance and fixed stuff
- Maintenance is not that good
- Catalog is on top of hive
- Cost to maintain database is low
- You can create crawlers and so on

## What is metadata in glue: Data from the data:
- File type (ex CSV);
- Location: s3://data-lake-raw/file.csv
- Compression: GZIP
- Schema: 
    customer_id, string
    created_at, date
    name, string
    ....
- Size: 2Mb

The metadata will be used by Athena, Spark and Presto.