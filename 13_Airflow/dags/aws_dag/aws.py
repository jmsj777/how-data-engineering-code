import boto3
from botocore import exceptions
from botocore.exceptions import ClientError
import logging
from dotenv import load_dotenv
from os import getenv
from abc import ABC
from datetime import datetime
import pandas as pd

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

load_dotenv('/opt/airflow/outputs/.env')
load_dotenv('/home/ze/.env')


class AWS_Airflow(ABC):
    def __init__(self) -> None:
        self.s3_client = boto3.client(
            's3',
            aws_access_key_id=getenv('AWS_ID'),
            aws_secret_access_key=getenv('AWS_key')
        )
        self.s3_resource = boto3.resource(
            's3',
            aws_access_key_id=getenv('AWS_ID'),
            aws_secret_access_key=getenv('AWS_key')
        )

    def _validate_bucket(self) -> bool:
        try:
            self.s3_client.list_buckets()['Buckets'][0]['Name']
        except IndexError:
            print('There is no bucket')
            return True
        return False

    def _create_bucket(self,name) -> bool:
        try:
            self.s3_client.create_bucket(Bucket=name)
        except ClientError as e:
            logging.error(e)
            return False
        return True
    
    def download_base(self):
        url = "https://s3-sa-east-1.amazonaws.com/ckan.saude.gov.br/Leitos/2021-07-17/esus-vepi.LeitoOcupacao.csv"
        df = pd.read_csv(url, low_memory=False)
        df.to_csv("/opt/airflow/outputs/dados.csv", index=False)

    def start(self) -> None:
        if self._validate_bucket():
            self._create_bucket('ze-s3-bucket-cleber')
            print(f'Bucket successfully created: ze-s3-bucket-cleber')
        else:
            name = self.s3_client.list_buckets()['Buckets'][0]['Name']
            print(f'Bucket already created: {name}')

    def send_file(self) -> None:
        data_proc=datetime.now().strftime("%Y%m%d_%H%M%S")
        self.s3_resource.Bucket('ze-s3-bucket-cleber').upload_file(
            '/opt/airflow/outputs/dados.csv',
            f'/opt/airflow/outputs/dados_{data_proc}.csv',
        )