import airflow
from airflow import DAG
from airflow.operators.python import PythonOperator
from datetime import datetime, timedelta
from aws_dag.aws import AWS_Airflow

aws = AWS_Airflow()

args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(0),
}

dag = DAG(
    dag_id='aws_covid',
    default_args=args,
    schedule_interval=None,
    dagrun_timeout=timedelta(minutes=20),
)

t1 = PythonOperator(
    task_id='download_base',
    python_callable=aws.download_base,
    dag=dag,
)

t2 = PythonOperator(
    task_id='start_bucket',
    python_callable=aws.start,
    dag=dag,
)

t3 = PythonOperator(
    task_id='load_data',
    python_callable=aws.send_file,
    dag=dag,
)


t1 >> t2 >> t3