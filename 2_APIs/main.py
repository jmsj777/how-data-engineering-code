# %%
import requests
import json
import backoff
import random
import logging

# %%
url = 'https://economia.awesomeapi.com.br/json/last/USD-BRL'
ret = requests.get(url)
# %%
if ret:
    print(ret.text)
else:
    print('Error!')
# %%
dolar = json.loads(ret.text)['USDBRL']

# %%
dolar['bid']
# %%
float(dolar['bid'])

# %%
print(f"20 dolars are worth {float(dolar['bid'])*20} reais ")

# %%


def quotation(value, coin):
    url = f'https://economia.awesomeapi.com.br/json/last/{coin}'
    # url = 'https://economia.awesomeapi.com.br/json/last/{}'.format(coin)
    ret = requests.get(url)
    coinVal = json.loads(ret.text)[coin.replace('-', '')]
    print(
        f"{value} {coin[:3]} are worth {round(float(coinVal['bid'])*value,2)} {coin[-3:]}")


quotation(20, 'USD-BRL')

# %%


def error_check(func):
    def inner_func(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except:
            print(f'{func.__name__} failed')
    return inner_func


@error_check
def multi_coins(value, coin):
    url = f'https://economia.awesomeapi.com.br/json/last/{coin}'
    ret = requests.get(url)
    coinVal = json.loads(ret.text)[coin.replace('-', '')]
    print(
        f"{value} {coin[:3]} are worth {round(float(coinVal['bid'])*value,2)} {coin[-3:]}")


multi_coins(20, 'USD-BRL')
multi_coins(20, 'EUR-BRL')
multi_coins(20, 'BTC-BRL')
multi_coins(20, 'RPL-BRL')
multi_coins(20, 'JPY-BRL')

# %%


@backoff.on_exception(backoff.expo, (ConnectionAbortedError, ConnectionRefusedError, TimeoutError), max_tries=10)
def test_func(*args, **kwargs):
    rnd = random.random()
    print(f"""
            RND: {rnd}  
            args: {args if args else 'no args'}  
            kwargs: {kwargs if kwargs else 'no kwargs'}  
        """)
    if rnd < .2:
        raise ConnectionAbortedError('Connection was finished')
    elif rnd < .4:
        raise ConnectionRefusedError('Connection was refused')
    elif rnd < .6:
        raise TimeoutError('The time limit was exceeded')
    else:
        return 'OK!'


# %%
test_func()
# %%
test_func(42)
# %%
test_func(42, 51, name='ze')
# %%
test_func(name='ze')

# %%
log = logging.getLogger()
log.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
ch = logging.StreamHandler()
ch.setFormatter(formatter)
log.addHandler(ch)
# %%


@backoff.on_exception(backoff.expo, (ConnectionAbortedError, ConnectionRefusedError, TimeoutError), max_tries=10)
def test_func(*args, **kwargs):
    rnd = random.random()
    log.debug(f" RND: {rnd} ")
    log.info(f"args: {args if args else 'no args'} ")
    log.info(f"kwargs: {kwargs if kwargs else 'no kwargs'}  ")
    if rnd < .2:
        log.error('Connection was finished')
        raise ConnectionAbortedError('Connection was finished')
    elif rnd < .4:
        log.error('Connection was refused')
        raise ConnectionRefusedError('Connection was refused')
    elif rnd < .6:
        log.error('The time limit was exceeded')
        raise TimeoutError('The time limit was exceeded')
    else:
        return 'OK!'

# %%
test_func()
# %%
