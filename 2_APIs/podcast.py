# %%
import requests
from bs4 import BeautifulSoup as bs
import logging
import pandas as pd

# %%
url = 'http://portalcafebrasil.com.br/todos/podcasts/'
res = requests.get(url)
# %%
res.text
# %%
soup = bs(res.text)
# %%
soup
# %%
soup.find('h5').text
# %%
soup.find('h5').a

# %%
soup.find('h5').a['href']
# %%
list_podcast = soup.find_all('h5')

# %%
for item in list_podcast:
    print(f"EP: {item.text} - Link: {item.a['href']}")
# %%
# outra maneira:
url = 'http://portalcafebrasil.com.br/todos/podcasts/page/{}/?ajax=true'
# %%
url.format(5)
# %%


def get_podcast(url):
    res = requests.get(url)
    soup = bs(res.text)
    return soup.find_all('h5')


# %%
get_podcast(url.format(5))
# %%
log = logging.getLogger()
log.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
ch = logging.StreamHandler()
ch.setFormatter(formatter)
log.addHandler(ch)
# %%

i = 1
list_podcast = []
list_get = get_podcast(url.format(i))
log.debug(f"Coletados {len(list_get)} episódios do link: {url.format(i)}")
while len(list_get) > 0:
    list_podcast = list_podcast + list_get
    i += 1
    list_get = get_podcast(url.format(i))
    log.debug(f"Coletados {len(list_get)} episódios do link: {url.format(i)}")

# %%
len(list_podcast)
# %%
df = pd.DataFrame(columns=['name','link'])
# %%
for item in list_podcast:
    df.loc[df.shape[0]] = [item.text, item.a['href']]

# %%
df.shape
# %%
df
# %%
df.to_csv('banco_de_podcasts.csv', sep=';', index=False)
# %%
