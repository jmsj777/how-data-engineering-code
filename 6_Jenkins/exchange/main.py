import requests
import json
from datetime import date, datetime


def quotation(value):
    url = f'https://economia.awesomeapi.com.br/json/last/USD-BRL'
    ret = requests.get(url)
    coinVal = json.loads(ret.text)['USDBRL']
    return float((coinVal['bid'])*value)


coin = quotation(1)
print(f"Current rate: {coin}")

with open('exchange.csv', 'a') as f:
    f.write(
        "{}:{}\n".format(datetime.strftime(
            datetime.now(), '%d/%m/%Y %H:%M'), coin)
    )
