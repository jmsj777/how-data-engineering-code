# To build image, run

```
docker build -t pyjenkins .
```

# To compose and get service running, do

```
docker-compose up
```

# alternatives

- Airflow
- Nifi
