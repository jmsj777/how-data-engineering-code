SELECT 
    t1.artist
    ,t1.song
FROM PUBLIC."Billboard" AS t1
ORDER BY t1.artist, t1.song 

SELECT DISTINCT 
    t1.artist,
    count(*) AS "#artist"
FROM PUBLIC."Billboard" AS t1
GROUP BY t1.artist 
ORDER BY 
    t1.artist
    --"#artist" DESC

SELECT DISTINCT 
    t1.song,
    count(*) AS "#song"
FROM PUBLIC."Billboard" AS t1
GROUP BY t1.song
ORDER BY 
    --t1.song
    "#song" DESC

    
    
SELECT DISTINCT t1.artist
    ,t2."#artist"
    ,t1.song
    ,t3."#song"
FROM PUBLIC."Billboard" AS t1
LEFT JOIN (
    SELECT DISTINCT t1.artist
        ,count(*) AS "#artist"
    FROM PUBLIC."Billboard" AS t1
    GROUP BY t1.artist
    ) AS t2 ON t1.artist = t2.artist
LEFT JOIN (
    SELECT DISTINCT t1.song
        ,count(*) AS "#song"
    FROM PUBLIC."Billboard" AS t1
    GROUP BY t1.song
    ) AS T3 ON t1.song = t3.song
ORDER BY t1.artist
    ,t1.song
    
    
 WITH cte_artist
AS (
    SELECT DISTINCT t1.artist
        ,count(*) AS "#artist"
    FROM PUBLIC."Billboard" AS t1
    GROUP BY t1.artist
    )
    ,cte_song
AS (
    SELECT DISTINCT t1.song
        ,count(*) AS "#song"
    FROM PUBLIC."Billboard" AS t1
    GROUP BY t1.song
    )
SELECT DISTINCT t1.artist
    ,t2."#artist"
    ,t1.song
    ,t3."#song"
FROM PUBLIC."Billboard" AS t1
LEFT JOIN cte_artist AS t2 ON t1.artist = t2.artist
LEFT JOIN cte_song AS T3 ON t1.song = t3.song
ORDER BY t1.artist
    ,t1.song