CREATE TABLE tb_web_site AS (
  WITH cte_dedup_artist AS (
    SELECT
      t1."date",
      t1."rank",
      t1.artist,
      ROW_NUMBER() OVER(
        PARTITION BY artist
        ORDER BY
          artist,
          "date"
      ) AS dedup
    FROM
      public."Billboard" AS t1
    ORDER BY
      t1.artist,
      t1."date"
  )
  SELECT
    t1."date",
    t1."rank",
    t1.artist
  FROM
    cte_dedup_artist AS t1
  WHERE
    t1.dedup = 1
);

CREATE TABLE tb_artist AS (
  SELECT
    t1."date",
    t1."rank",
    t1.artist,
    t1.song,
    ROW_NUMBER() OVER(
      PARTITION BY artist
      ORDER BY
        artist,
        "date"
    )
  FROM
    public."Billboard" AS t1
  WHERE
    t1.artist = 'AC/DC'
  ORDER BY
    t1.artist,
    t1.song,
    t1."date"
);

DROP TABLE tb_artist;

SELECT
  *
FROM
  tb_artist;

create view vw_artist as (
  WITH cte_dedup_artist AS (
    SELECT
      t1."date",
      t1."rank",
      t1.artist,
      ROW_NUMBER() OVER(
        PARTITION BY artist
        ORDER BY
          artist,
          "date"
      ) AS dedup
    FROM
      tb_artist AS t1
    ORDER BY
      t1.artist,
      t1."date"
  )
  SELECT
    t1."date",
    t1."rank",
    t1.artist
  FROM
    cte_dedup_artist AS t1
  WHERE
    t1.dedup = 1
);

DROP VIEW vw_artist;

SELECT
  *
FROM
  vw_artist;

insert into
  tb_artist (
    SELECT
      t1."date",
      t1."rank",
      t1.artist,
      t1.song
    FROM
      public."Billboard" AS t1
    where
      t1.artist like 'Adele%'
    ORDER BY
      t1.artist,
      t1.song,
      t1."date"
  );

SELECT
  *
FROM
  tb_artist;

create view vw_song as (
  WITH cte_dedup_song AS (
    SELECT
      t1."date",
      t1."rank",
      t1.artist,
      t1.song,
      ROW_NUMBER() OVER(
        PARTITION BY artist,
        song
        ORDER BY
          artist,
          song,
          "date"
      ) AS dedup
    FROM
      tb_artist AS t1
    ORDER BY
      t1.artist,
      t1.song,
      t1."date"
  )
  SELECT
    t1."date",
    t1."rank",
    t1.artist,
    t1.song
  FROM
    cte_dedup_song AS t1
  WHERE
    t1.dedup = 1
);

SELECT
  *
FROM
  vw_artist;

SELECT
  *
FROM
  vw_song;

CREATE view vw_song as (
  WITH cte_dedup_song AS (
    SELECT
      t1."date",
      t1."rank",
      t1.song,
      ROW_NUMBER() OVER(
        PARTITION BY song
        ORDER BY
          song,
          "date"
      ) AS dedup
    FROM
      tb_artist AS t1
    ORDER BY
      t1.song,
      t1."date"
  )
  SELECT
    t1."date",
    t1."rank",
    t1.song
  FROM
    cte_dedup_song AS t1
  WHERE
    t1.dedup = 1
);

DROP VIEW vw_song;