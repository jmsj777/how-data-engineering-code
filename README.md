# How Bootcamps - Engenharia de Dados

- Here is my iteration of the How Bootcamps - Data Engineering course
I followed along with all of the classes and made the prototypes to get some concepts of:
- Data Scrapping;
- API data threatment;
- SQL;
- OOP in Python;
- Checkpoints;
- TDD and Pytest;
- Poetry for dependency management with virtualenv;
- Selenium for end-to-end testing;
- Jenkins for scheduling;
- GIT and Github Actions for CI, as I did it in Gitlab, it was adapted for it;
- AWS Services and capabilities overview;
- Github Actions for CD for AWS;
- Data lakes with AWS S3;
- AWS Glue and spark for data pipeline;
- Apache Airflow as an Jenkins alternative;
- Infrastructure as code with Terraform and Cloudformation;
- The live encounters had a lot of different topics, from interview proccess to machine learning, some of it is in the /live folder;
- There is also some code done to test some data manipulation with json by pandas in the /play folder.

## 1_Loterias

- this is an app that analyses the official gambling data in brazilian lotteries, it gets the most recurring value, the less recurring, and also get some combinations and when it is most occurred

### Installation

- With python installed, run:

```
$ pip install virtualenv
```

- Then create the virtual enviroment:

```
$ python -m venv .venv
```

- Activate the env

```
$ . .venv/Scripts/activate
```

- Install the dependencies

```
$ pip install -r .\requirements.txt
```

### Running the app

To run you just need to run:

```
$ python main.py '<here the link goes>'
```

For building and running the app we used the official history from the brazilian bank's website: [here](http://loterias.caixa.gov.br/wps/portal/loterias/landing/lotofacil/!ut/p/a1/04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbz8vTxNDRy9_Y2NQ13CDA0sTIEKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wBmoxN_FydLAGAgNTKEK8DkRrACPGwpyQyMMMj0VAcySpRM!/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_HGK818G0K85260Q5OIRSC42046/res/id=historicoHTML/c=cacheLevelPage/=/)
