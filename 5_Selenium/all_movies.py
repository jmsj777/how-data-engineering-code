# %%
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
import pandas as pd

s = Service('.\src\chromedriver.exe')
driver = webdriver.Chrome(service=s)


def has_item(xpath, driver=driver):
    try:
        driver.find_element(By.XPATH, xpath)
        return True
    except:
        return False


driver.implicitly_wait(5)

driver.get('https://pt.wikipedia.org/wiki/Nicolas_Cage')

table_path = '//*[@id="mw-content-text"]/div[1]/table[2]'

i = 0
while not has_item(table_path):
    i += 1
    if i > 50:
        break
    pass

table = driver.find_element(By.XPATH, table_path)

df = pd.read_html('<table>'+table.get_attribute('innerHTML') + '</table>')[0]

with open('print.png', 'wb') as f:
    f.write(driver.find_element(By.XPATH, '/html/body').screenshot_as_png) 

driver.close()

df.to_csv('movies_Nicolas_Cage.csv', sep=';', index=False)

# %%
