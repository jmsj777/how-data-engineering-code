provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_s3_bucket" "bucket_production" {
  bucket = "bucket-do-cleber-production"
  tags = {
    "Area" = "Products"
    "Environment" = "Production"
  }
}

resource "aws_s3_bucket" "bucket_development" {
  bucket = "bucket-do-cleber-development"
  tags = {
    "Area" = "Products"
    "Environment" = "development"
  }
}
