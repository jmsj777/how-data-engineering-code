import requests
import pandas as pd
import collections
import sys

# url = 'http://loterias.caixa.gov.br/wps/portal/loterias/landing/lotofacil/!ut/p/a1/04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbz8vTxNDRy9_Y2NQ13CDA0sTIEKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wBmoxN_FydLAGAgNTKEK8DkRrACPGwpyQyMMMj0VAcySpRM!/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_HGK818G0K85260Q5OIRSC42046/res/id=historicoHTML/c=cacheLevelPage/=/'
url = sys.argv[1]

r = requests.get(url)

r_text = r.text

df = pd.read_html(r_text)

type(df)
type(df[0])

df = df[0].copy()

nr_pop = list(range(1, 26))
nr_evens = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24]
nr_odds = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25]
nr_primes = [2, 3, 5, 7, 11, 13, 17, 19, 23]

comb = []
v_01 = 0
v_02 = 0
v_03 = 0
v_04 = 0
v_05 = 0
v_06 = 0
v_07 = 0
v_08 = 0
v_09 = 0
v_10 = 0
v_11 = 0
v_12 = 0
v_13 = 0
v_14 = 0
v_15 = 0
v_16 = 0
v_17 = 0
v_18 = 0
v_19 = 0
v_20 = 0
v_21 = 0
v_22 = 0
v_23 = 0
v_24 = 0
v_25 = 0

list_fields = ['Bola1', 'Bola2', 'Bola3', 'Bola4', 'Bola5', 'Bola6', 'Bola7',
               'Bola8', 'Bola9', 'Bola10', 'Bola11', 'Bola12', 'Bola13', 'Bola14', 'Bola15']


for index, row in df.iterrows():
    v_evens = 0
    v_odds = 0
    v_primes = 0

    for field in list_fields:
        if row[field] in nr_evens:
            v_evens += 1
        if row[field] in nr_odds:
            v_odds += 1
        if row[field] in nr_primes:
            v_primes += 1
        if row[field] == 1:
            v_01 += 1
        if row[field] == 2:
            v_02 += 1
        if row[field] == 3:
            v_03 += 1
        if row[field] == 4:
            v_04 += 1
        if row[field] == 5:
            v_05 += 1
        if row[field] == 6:
            v_06 += 1
        if row[field] == 7:
            v_07 += 1
        if row[field] == 8:
            v_08 += 1
        if row[field] == 9:
            v_09 += 1
        if row[field] == 10:
            v_10 += 1
        if row[field] == 11:
            v_11 += 1
        if row[field] == 12:
            v_12 += 1
        if row[field] == 13:
            v_13 += 1
        if row[field] == 14:
            v_14 += 1
        if row[field] == 15:
            v_15 += 1
        if row[field] == 16:
            v_16 += 1
        if row[field] == 17:
            v_17 += 1
        if row[field] == 18:
            v_18 += 1
        if row[field] == 19:
            v_19 += 1
        if row[field] == 20:
            v_20 += 1
        if row[field] == 21:
            v_21 += 1
        if row[field] == 22:
            v_22 += 1
        if row[field] == 23:
            v_23 += 1
        if row[field] == 24:
            v_24 += 1
        if row[field] == 25:
            v_25 += 1
    comb.append(str(v_evens) + ' pares, ' + str(v_odds) +
                ' ímpares e ' + str(v_primes) +  'primos')

freq_nr = [
    [1, v_01],
    [2, v_02],
    [3, v_03],
    [4, v_04],
    [5, v_05],
    [6, v_06],
    [7, v_07],
    [8, v_08],
    [9, v_09],
    [10, v_10],
    [11, v_11],
    [12, v_12],
    [13, v_13],
    [14, v_14],
    [15, v_15],
    [16, v_16],
    [17, v_17],
    [18, v_18],
    [19, v_19],
    [20, v_20],
    [21, v_21],
    [22, v_22],
    [23, v_23],
    [24, v_24],
    [25, v_25]
]

freq_nr.sort(key=lambda tup: tup[1])
freq_nr[0]
freq_nr[-1]  # last item

counter = collections.Counter(comb)
results = pd.DataFrame(counter.items(), columns=['Combination', 'Frequency'])
results['p_freq'] = results['Frequency']/results['Frequency'].sum()
results = results.sort_values(by='p_freq')

print('''
    O número mais frequente é o: {}
    O número menos frequente é o: {}
    A combinação de pares, ímpares e primos mais frequente é: {} com a frequencia de: {}%
'''.format(freq_nr[-1][0], freq_nr[0][0], results['Combination'].values[-1], int((results['p_freq'].values[-1]*100)*100)/100))
