
import datetime
import math
from typing import List


class Person:
    def __init__(self, name: str, last_name: str,
                 date_of_birth: datetime.date):
        self.date_of_birth = date_of_birth
        self.last_name = last_name
        self.name = name

    @property
    def age(self) -> int:
        return math.floor((datetime.date.today() - self.date_of_birth).days / 365.2425)

    def __str__(self) -> str:
        return f"{self.name} {self.last_name} is {self.age} years old"


class Curriculum:
    def __init__(self, person: Person, experiences: List[str]):
        self.experiences = experiences
        self.person = person

    @property
    def quantity_of_experiences(self) -> int:
        return len(self.experiences)

    @property
    def current_job(self) -> str:
        return self.experiences[-1]

    def add_experience(self, experience: str) -> None:
        self.experiences.append(experience)

    def __str__(self) -> str:
        return f"{self.person} and " \
            f"worked at {self.quantity_of_experiences} companies " \
            f"and now work at {self.current_job}"


ze = Person(name='Ze', last_name='da Silva',
            date_of_birth=datetime.date(1997, 6, 16))
print(ze)
curriculum_ze = Curriculum(person=ze, experiences=[
                           'Unika', 'TBA', 'Fach', 'Bravo', 'Yves Tech'])
print(curriculum_ze)
curriculum_ze.add_experience('Seilah')
print(curriculum_ze)


class Living_being:
    def __init__(self, name: str,
                 date_of_birth: datetime.date):
        self.date_of_birth = date_of_birth
        self.name = name

    @property
    def age(self) -> int:
        return math.floor((datetime.date.today() - self.date_of_birth).days / 365.2425)

    def make_noise(self, noise: str) -> str:
        print(f"{self.name} make noise: {noise}")


class PersonInheritance(Living_being):
    def __str__(self) -> str:
        return f"{self.name} is {self.age} years old"

    def talk(self, phrase):
        return self.make_noise(phrase)


ze2 = PersonInheritance(name='ze', date_of_birth=datetime.date(1997, 6, 16))

print(ze2)


class Dog(Living_being):
    def __init__(self, name: str, date_of_birth: datetime.date, breed: str):
        super().__init__(name, date_of_birth)
        self.breed = breed

    def __str__(self) -> str:
        return f"{self.name} has the breed {self.breed} is {self.age} years old"

    def bark(self):
        return self.make_noise('Woof! Woof! ')

skoob = Dog(name='Skoob', date_of_birth=datetime.date(
    2009, 5, 10), breed='SRD')

print(skoob)

skoob.bark()
skoob.bark()
skoob.bark()
skoob.bark()
ze2.talk('Seilah')
skoob.bark()